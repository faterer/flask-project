FROM python:3.8-alpine
WORKDIR /app
COPY ./requirements.txt ./requirements-server.txt .
RUN pip install --no-cache-dir -r requirements.txt && \
    pip install --no-cache-dir -r requirements-server.txt
COPY . .
CMD [ "gunicorn", "app:app", "-b", "0.0.0.0:8000" ]

